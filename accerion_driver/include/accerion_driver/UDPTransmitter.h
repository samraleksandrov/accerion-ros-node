/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef UDPTRANSMITTER_H_
#define UDPTRANSMITTER_H_

#include <iostream>
#include <cstring>
#include <vector>
#include <array>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <net/route.h>
#include <netdb.h>
#include <unistd.h>

#include "NetworkConstants.h"

/**
 * @brief      Wrapper class that holds a UDP socket for transmitting messages.
 */
class UDPTransmitter
{
public:

    /**
     * @brief      Constructs a UDPTransmitter object
     *
     * @param[in]  remoteReceivePort  The remote receive port to which the outgoing UDP messages are sent
     */
    UDPTransmitter(unsigned int remoteReceivePort);
    ~UDPTransmitter();
    /**
     * @brief      Transmits a UDP Message, pointed by transmittedMessage and
     *             has byte size transmittedNumOfBytes
     *
     * @param      transmittedMessage     The transmitted message
     * @param[in]  transmittedNumOfBytes  The transmitted number of bytes
     *
     * @return     True if UDP message is sent successfully, false otherwise
     */
    bool transmitMessage(uint8_t* transmittedMessage, unsigned int transmittedNumOfBytes);

    bool setMulticastIPAddress(struct in_addr multicastAddress);

    void setBroadcastIPAddress(struct in_addr broadcastAddress);

    inline sockaddr_in getRemoteIPAddress(){return remoteAddress_;};

	void setReceiver(const std::string ip);
private:
    unsigned int remoteReceivePort_;/** < Holds the remote receive port to which the outgoing UDP
                                     * messages are sent */
    int socketEndpoint_;/** < Holds the file descriptor for UDP socket
                         */
    struct sockaddr_in remoteAddress_;/** < Holds the remote address to which outgoing UDP messages are
                                       * sent (now set to Broadcast) */
    
    static constexpr unsigned int bufferSize_ = NetworkConstants::maximumNumOfBytesInUDPMessage;
    
    bool debugMode_;/** < Turns on/off debug console messages */
};
#endif


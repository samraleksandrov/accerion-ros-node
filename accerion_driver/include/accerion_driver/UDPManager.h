/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef UDPMANAGER_H_
#define UDPMANAGER_H_

#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstring>

#include "UDPReceiver.h"
#include "UDPTransmitter.h"
#include "CRC8.h"
#include "Commands.h"

#define IS_BIG_ENDIAN (*(uint16_t *)"\0\xff" < 0x100)
#define DEFAULT_SERIAL_NUMBER 0xFFFFFFFF

#define JUPITER_RECEIVE_PORT    13360
#define REMOTE_RECEIVE_PORT     13359

class UDPManager
{
public:
    
    /**
     * @brief      Constructs UDP Manager object
     *
     * @param[in]  receivePort        The receive port for incoming UDP Messages
     * @param[in]  remoteReceivePort  The remote receive port for outgoing UDP
     *                                Messages
     */
    UDPManager(int jupiterSerial);

    ~UDPManager();

    /*************************************************************************/
    /*OVERLOADED MEMBERS*/
    /*************************************************************************/
    friend std::ostream &operator<<( std::ostream &output, const UDPManager &udpManager);
    /*************************************************************************/
    /************************************/
    
    /**
     * @brief      Reads one incoming UDP message
     */
    bool receiveMessage();

    /**
     * @brief      Sends a boolean UDP acknowledge message.
     *
     * @param[in]  commandID  The command id
     * @param[in]  ackValue   The acknowledge value (true or false)
     *
     * @return     True if message sent successfully, false otherwise
     */
    bool sendBooleanAcknowledgeMessage(uint8_t commandID, bool ackValue);
        
    /**
     * @brief      Sends a raw data message without any predefined format
     *
     * @param[in]  commandID  The command id
     * @param[in]  rawData    The raw data
     * @param[in]  length     The length of raw data in bytes
     *
     * @return     True if message sent successfully, false otherwise
     */
    bool sendRawDataMessage(uint8_t commandID, const uint8_t *rawData, unsigned int length);

    bool sendDataMessage(uint8_t commandID, const std::vector<uint8_t> data);

    bool sendUDPMessageToJupiter(const uint8_t commandID, const std::vector<uint8_t>& data);

    /**
     * @brief      Sends a basic acknowledge message with only serial number,
     *             command ID and CRC code.
     *
     * @param[in]  commandID  The command id
     *
     * @return     True if message sent successfully, false otherwise
     */
    bool sendBasicAcknowledgeMessage(uint8_t commandID);

    bool sendExternalReferenceMessage(uint8_t commandID, const uint64_t timeOffsetInUSecs, 
                             const float &absPoseX, const float &absPoseY, const float &absPoseTheta, 
                             const float &covPoseX, const float &covPoseY, const float &covPoseTheta);

    void setReceiver(const std::string ip);

    void setSerialNumber(const int serialNumber);

    bool setJupiterTimeAndDate();

    /**
     * @brief      serializes a uint16_t (i.e. short) into a byte (uint8_t)
     *             array in network-byte order (Big-Endian)
     *
     * @param[in]  in         input integer as uint16_t
     * @param      out        Pointer to the resulting byte array of size 2
     * @param[in]  bigEndian  True if input is Big-Endian, false otherwise
     */
    void serializeUInt16(uint16_t in, uint8_t* out, bool bigEndian = IS_BIG_ENDIAN);
    
    /**
     * @brief      serializes a uint32_t (i.e. int) into a byte (uint8_t) array
     *             in network-byte order (Big-Endian)
     *
     * @param[in]  in         input integer as uint32_t
     * @param      out        Pointer to the resulting byte array of size 4
     * @param[in]  bigEndian  True if input is Big-Endian, false otherwise
     */
    void serializeUInt32(uint32_t in, uint8_t* out, bool bigEndian = IS_BIG_ENDIAN);
    
    /**
     * @brief      serializes a uint64_t (i.e. long) into a byte (uint8_t) array
     *             in network-byte order (Big-Endian)
     *
     * @param[in]  in         input integer as uint64_t
     * @param      out        Pointer to the resulting byte array of size 8
     * @param[in]  bigEndian  True if input is Big-Endian, false otherwise
     */
    void serializeUInt64(uint64_t in, uint8_t* out, bool bigEndian = IS_BIG_ENDIAN);

    /**
     * @brief      Gets the received command id.
     *
     * @return     The received command id.
     */
    inline uint8_t getReceivedCommandID()
    {
        return receivedCommandID_;
    }

    /**
     * @brief      Gets the received command as a uint8_t pointer.
     *
     * @return     A pointer to the received command.
     */
    inline uint8_t* getReceivedCommand()
    {
        return receivedCommand_.data();
    }

    /**
     * @brief      Gets the number of received bytes.
     *
     * @return     the number of received bytes.
     */
    inline unsigned int getReceivedNumberOfBytes()
    {
        return receivedNumOfBytes_;
    }

    /**
     * @brief      Gets the serial number.
     *
     * @return     the serial number.
     */
    inline uint32_t getReceivedSerialNumber()
    {
        return receivedSerialNumber_;
    }

    /**
     * @brief      Gets the received command as a std::string.
     *
     * @return     The received command as std::string.
     */
    inline std::string getReceivedCommandString()
    {
        // std::cout << receivedCommand_.size() << "  :  " << receivedNumOfBytes_ << std::endl;
        std::string inputString = std::string(receivedCommand_.begin(), receivedCommand_.end());
        // std::cout << inputString.size() << "  :" << inputString << ":"<< std::endl;
        return inputString;
    }

    /**
     * @brief      Gets the received message as a uint8_t pointer.
     *
     * @return     A pointer to the received message.
     */
    inline uint8_t* getReceivedMessage()
    {
        return receivedMessage_;
    }

    std::string getUDPBinaryMessageInString();

    std::string receivedIpAddressStr_;

private:

    /**
     * @brief      forms a UDP message according to specification (i.e. adding
     *             serial number, UDP command ID and CRC8 code) and sends it
     *             using UDPTransmitter
     *
     * @return     True if UDP message sent successfully, false otherwise
     */
    bool formAndSendMessage();

    void setCharFrom64(char *result, uint64_t number);

    void setCharFrom32(char *result, uint32_t number);

    void setCharFrom16(char *result, uint16_t number);


    /*SERIAL NUMBER*/
    uint32_t serialNumber_;                          /** < Holds the unique serial number of the Jupiter unit*/

    /*AGGREGATED OBJECTS*/
    UDPReceiver*    udpReceiver_;                    /** < Pointer to the wrapper object of UDP receiving socket */
    UDPTransmitter* udpTransmitter_;                 /** < Pointer to the wrapper object of UDP transmitting socket */

    /*RECEIVED MESSAGE*/
    uint32_t        receivedSerialNumber_;           /** < Holds received serial number from incoming UDP message, used to
                                                         * determine if message is intended for this unit */
    uint8_t*        receivedMessage_;               /** < Points to the byte array of incoming UDP message */
    unsigned int    receivedNumOfBytes_;             /** < Holds number of bytes in incoming UDP message */
    std::vector<uint8_t>    receivedCommand_;        /** < Holds incoming UDP command (size depends on UDP command ID) */
    uint8_t         receivedCommandID_;              /** < Holds incoming UDP command ID */
    uint8_t         receivedCRC8_;                   /** < Holds incoming CRC8 code (in 0xD8) , compared with CRC code
                                                         * computed at Jupiter side for error-checking */

    /*TRANSMITTED MESSAGE*/
    uint8_t         transmittedSerialNumberData_[4]; /** < Holds the byte array for outgoing serial number */
    uint8_t         transmittedTimeStamp_[8];        /** < Holds outgoing timestamp value as a byte array */
    uint32_t        transmittedSerialNumber_;        /** < Holds the serial number as a single uint32_t */
    uint8_t         transmittedCommandID_;           /** < Holds outgoing UDP command ID */
    unsigned int    transmittedNumOfBytes_;          /** < Holds number of bytes in outgoing UDP message */
    uint8_t         transmittedCRC8_;                /** < Holds CRC8 code (in 0xD8) of outgoing UDP message*/
    std::vector<uint8_t>    transmittedData_;        /** < Holds outgoing UDP data (i.e. part of the message excluding
                                                         * serial number, command ID and CRC code) */
    std::vector<uint8_t>    transmittedMessage_;     /** < Holds the entire outgoing UDP message */

    /*CRC8*/
    CRC8 crc8_;                                      /** < Object of CRC8 class, used to determine CRC code (in 0xD8) */

    /*DEBUG MODE*/
    bool debugMode_;                                 /** < Turns on/off debug messages */
    bool debugModeStreaming_;                        /** < Turns on/off debug messages for streaming UDP messages*/
};


#endif

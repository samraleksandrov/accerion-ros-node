/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef COMMANDS_H_
#define COMMANDS_H_

#include <map>

/**
 * @brief      Holds UDP Command sample flags, cmdIDs in hexadecimal format and cmdNames
 */

namespace Commands 
{
    enum CommandFlags
    {
        cmdTrue              = 0x01,
        cmdFalse             = 0x02
    };

    enum CommandFields
    {
        SERIAL_NUMBER_BEGIN_BYTE    = 0,
        SERIAL_NUMBER_LENGTH        = 4,
        COMMAND_ID_BEGIN_BYTE       = 4,
        COMMAND_ID_LENGTH           = 1,
        CRC_LENGTH                  = 1,
        MESSAGE_SIZE_LENGTH         = 4
    };

    enum CommandIDs
    {
        PRD_HEARTBEAT_INFO                      = 0x01,
        STR_CORRECTED_POSE_DATA                 = 0x10,
        STR_CORRECTED_POSE_DATA_LIGHT           = 0x11,
        STR_UNCORRECTED_POSE_DATA               = 0x12,
        STR_DIAGNOSTICS                         = 0x13,
        INT_DRIFT_CORRECTION_DONE               = 0x14,
        STR_QUALITY_ESTIMATE                    = 0x15,
        STR_LINE_FOLLOWER                       = 0x16,
        STR_SIGNATURE_MARKER                    = 0x17,
        INT_ARUCO_MARKER                        = 0x18,
        ACK_MAPPING_MODE                        = 0x20,
        ACK_QR_DETECTION_MODE                   = 0x21,
        ACK_LOCALIZATION_MODE                   = 0x22,
        ACK_RECORDING_MODE                      = 0x23,
        ACK_IDLE_MODE                           = 0x24,
        ACK_RESET_MODE                          = 0x25,
        ACK_CALIBRATION_MODE                    = 0x27,
        ACK_COMPLETE_QR_LIBRARY_REMOVED         = 0x28,
        ACK_COMPLETE_CLUSTER_LIBRARY_REMOVED    = 0x29,
        ACK_RECOVERY_MODE                       = 0x2A,
        ACK_ARUCO_MARKER_DETECTION_MODE         = 0x2B,
        ACK_LINE_FOLLOWER_MODE                  = 0x2D,
        ACK_SIGNATURE_MARKER_MAP_START_STOP     = 0x2E,
        ACK_REPLACE_CLUSTER_G2O                 = 0x2F,
        ACK_QR_ADDED_TO_LIBRARY                 = 0x30,
        ACK_QR_REMOVED_FROM_LIBRARY             = 0x31,
        ACK_SAMPLE_RATE                         = 0x32,
        ACK_DRIFT_CORRECTION_MISSED             = 0x33,
        ACK_CLUSTER_REMOVED                     = 0x34,
        ACK_SOFTWARE_HASH                       = 0x3A,
        ACK_IP_ADDRESS                          = 0x40,
        ACK_TCPIP_INFO                          = 0x41,
        ACK_NEW_POSITION_IS_SET                 = 0x42,
        ACK_CALIBRATION_INFO                    = 0x43,
        ACK_SERIAL_NUMBER                       = 0x44,
        ACK_CONSOLE_OUTPUT_INFO                 = 0x45,
        ACK_JUPITER_STARTED                     = 0x46,
        ACK_JUPITER_STOPPED                     = 0x47,
        ACK_TIME_AND_DATE                       = 0x48,                                         
        ACK_SENSOR_MOUNT_POSE                   = 0x49,
        ACK_SOFTWARE_VERSION                    = 0x4D,
        ACK_UDP_INFO                            = 0x4E,
        ACK_CLUSTER_G2O                         = 0x4F,
        CMD_SET_LEARNING_MODE                   = 0x50,
        CMD_SET_QR_DETECTION                    = 0x51,        
        CMD_SET_DRIFT_CORRECTION_MODE           = 0x52,
        CMD_SET_RECORDING_MODE                  = 0x53,
        CMD_SET_IDLE_MODE                       = 0x54,
        CMD_SET_RESET_MODE                      = 0x55,
        CMD_SET_CALIBRATION_MODE                = 0x56,
        CMD_GET_SIGNATURE_MARKER_MAP            = 0x5A,
        CMD_SET_ARUCO_MARKER_MODE               = 0x5B, 
        CMD_GET_IP_ADDRESS                      = 0x60,
        CMD_GET_ALL_IP_ADDRESSES                = 0x61,
        CMD_GET_SAMPLE_RATE                     = 0x62,
        CMD_REMOVE_COMPLETE_QR_LIBRARY          = 0x63,
        CMD_DOWNLOAD_QR_LIBRARY                 = 0x64,
        CMD_GET_ALL_SERIAL_NUMBERS              = 0x65,                 
        CMD_REMOVE_COMPLETE_CLUSTER_LIBRARY     = 0x66,
        CMD_GET_ALL_ACKNOWLEDGEMENTS            = 0x67,
        CMD_GET_SOFTWARE_VERSION                = 0x68,
        CMD_GET_TCPIP_RECEIVER                  = 0x69,
        CMD_GET_SENSOR_MOUNT_POSE               = 0x6A,
        CMD_SET_SAMPLE_RATE                     = 0x70,
        CMD_REMOVE_QR_FROM_LIBRARY              = 0x71,
        CMD_SET_RECOVERY_MODE                   = 0x72,
        CMD_DELETE_CLUSTER                      = 0x74,
        CMD_SET_IP_ADDRESS                      = 0x80,
        CMD_SET_NEW_POSE                        = 0x81,
        CMD_ADD_QR_TO_LIBRARY                   = 0x82,
        CMD_SET_TIME_DATE                       = 0x83,
        CMD_SET_SENSOR_MOUNT_POSE               = 0x84,
        CMD_SET_POSE_AND_COVARIANCE             = 0x85,
        CMD_START_MARKERLESS_LEARNING           = 0x86,
        CMD_SET_TCPIP_RECEIVER                  = 0x87,
        CMD_SET_LINE_FOLLOWER                   = 0x88,
        CMD_SET_UDP_SETTINGS                    = 0x89,
        CMD_GET_CLUSTER_G2O                     = 0x90,
        CMD_REPLACE_CLUSTER_G2O                 = 0x91,
        CMD_GET_SOFTWARE_HASH                    = 0x97
    };

    enum class MessageTypes : int
    {
        PERIODIC        = 0X01,
        STREAMING       = 0x02,
        INTERMITTENT    = 0x03,
        ACKNOWLEDGEMENT = 0x04,
        COMMAND         = 0x05
    };

    const std::map<uint8_t, std::tuple<std::string, unsigned int, MessageTypes>> commandValues
    {
        {PRD_HEARTBEAT_INFO                     , std::tuple<std::string, unsigned int, MessageTypes> {"PRD_HEARTBEAT_INFO"                     , 14,   MessageTypes::PERIODIC}},
        {STR_CORRECTED_POSE_DATA                , std::tuple<std::string, unsigned int, MessageTypes> {"STR_CORRECTED_POSE_DATA"                , 81,   MessageTypes::STREAMING }},
        {STR_CORRECTED_POSE_DATA_LIGHT          , std::tuple<std::string, unsigned int, MessageTypes> {"STR_CORRECTED_POSE_DATA_LIGHT"          , 61,   MessageTypes::STREAMING }},
        {STR_UNCORRECTED_POSE_DATA              , std::tuple<std::string, unsigned int, MessageTypes> {"STR_UNCORRECTED_POSE_DATA"              , 49,   MessageTypes::STREAMING }},
        {STR_DIAGNOSTICS                        , std::tuple<std::string, unsigned int, MessageTypes> {"STR_DIAGNOSTICS"                        , 6,    MessageTypes::STREAMING }},
        {INT_DRIFT_CORRECTION_DONE              , std::tuple<std::string, unsigned int, MessageTypes> {"INT_DRIFT_CORRECTION_DONE"              , 54,   MessageTypes::INTERMITTENT}},
        {STR_QUALITY_ESTIMATE                   , std::tuple<std::string, unsigned int, MessageTypes> {"STR_QUALITY_ESTIMATE"                   , 16,   MessageTypes::STREAMING }},
        {STR_LINE_FOLLOWER                      , std::tuple<std::string, unsigned int, MessageTypes> {"STR_LINE_FOLLOWER"                      , 34,   MessageTypes::STREAMING }},
        {STR_SIGNATURE_MARKER                   , std::tuple<std::string, unsigned int, MessageTypes> {"STR_SIGNATURE_MARKER"                   , 0,    MessageTypes::STREAMING }},
        {INT_ARUCO_MARKER                       , std::tuple<std::string, unsigned int, MessageTypes> {"INT_ARUCO_MARKER"                       , 28,   MessageTypes::INTERMITTENT }},
        {ACK_MAPPING_MODE                       , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_MAPPING_MODE"                       , 7,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_QR_DETECTION_MODE                  , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_QR_DETECTION_MODE"                  , 7,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_LOCALIZATION_MODE                  , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_LOCALIZATION_MODE"                  , 7,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_RECORDING_MODE                     , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_RECORDING_MODE"                     , 7,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_IDLE_MODE                          , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_IDLE_MODE"                          , 7,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_RESET_MODE                         , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_RESET_MODE"                         , 7,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_CALIBRATION_MODE                   , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_CALIBRATION_MODE"                   , 7,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_COMPLETE_QR_LIBRARY_REMOVED        , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_COMPLETE_QR_LIBRARY_REMOVED"        , 7,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_COMPLETE_CLUSTER_LIBRARY_REMOVED   , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_COMPLETE_CLUSTER_LIBRARY_REMOVED"   , 7,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_RECOVERY_MODE                      , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_RECOVERY_MODE"                      , 7,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_ARUCO_MARKER_DETECTION_MODE        , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_ARUCO_MARKER_DETECTION_MODE"        , 7,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_LINE_FOLLOWER_MODE                 , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_LINE_FOLLOWER_MODE"                 , 7,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_SIGNATURE_MARKER_MAP_START_STOP    , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_SIGNATURE_MARKER_MAP_START_STOP"    , 7,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_REPLACE_CLUSTER_G2O                , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_REPLACE_CLUSTER_G2O"                , 7,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_QR_ADDED_TO_LIBRARY                , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_QR_ADDED_TO_LIBRARY"                , 8,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_QR_REMOVED_FROM_LIBRARY            , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_QR_REMOVED_FROM_LIBRARY"            , 8,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_SAMPLE_RATE                        , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_SAMPLE_RATE"                        , 8,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_DRIFT_CORRECTION_MISSED            , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_DRIFT_CORRECTION_MISSED"            , 8,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_CLUSTER_REMOVED                    , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_CLUSTER_REMOVED"                    , 8,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_IP_ADDRESS                         , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_IP_ADDRESS"                         , 26,   MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_TCPIP_INFO                         , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_TCPIP_INFO"                         , 15,   MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_NEW_POSITION_IS_SET                , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_NEW_POSITION_IS_SET"                , 18,   MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_CALIBRATION_INFO                   , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_CALIBRATION_INFO"                   , 0,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_SERIAL_NUMBER                      , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_SERIAL_NUMBER"                      , 10,   MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_CONSOLE_OUTPUT_INFO                , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_CONSOLE_OUTPUT_INFO"                , 0,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_JUPITER_STARTED                    , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_JUPITER_STARTED"                    , 6,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_JUPITER_STOPPED                    , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_JUPITER_STOPPED"                    , 6,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_TIME_AND_DATE                      , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_TIME_AND_DATE"                      , 13,   MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_SENSOR_MOUNT_POSE                  , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_SENSOR_MOUNT_POSE"                  , 18,   MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_SOFTWARE_VERSION                   , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_SOFTWARE_VERSION"                   , 9,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_UDP_INFO                           , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_UDP_MULTICAST"                      , 12,   MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_CLUSTER_G2O                        , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_CLUSTER_G2O"                        , 0,    MessageTypes::ACKNOWLEDGEMENT}},
        {ACK_SOFTWARE_HASH                      , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_SOFTWARE_HASH"                      , 57,   MessageTypes::ACKNOWLEDGEMENT}},
        {CMD_SET_LEARNING_MODE                  , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_LEARNING_MODE"                  , 7,    MessageTypes::COMMAND}},
        {CMD_SET_QR_DETECTION                   , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_QR_DETECTION"                   , 7,    MessageTypes::COMMAND}},
        {CMD_SET_DRIFT_CORRECTION_MODE          , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_DRIFT_CORRECTION_MODE"          , 7,    MessageTypes::COMMAND}},
        {CMD_SET_RECORDING_MODE                 , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_RECORDING_MODE"                 , 7,    MessageTypes::COMMAND}},
        {CMD_SET_IDLE_MODE                      , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_IDLE_MODE"                      , 7,    MessageTypes::COMMAND}},
        {CMD_SET_RESET_MODE                     , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_RESET_MODE"                     , 7,    MessageTypes::COMMAND}},
        {CMD_SET_CALIBRATION_MODE               , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_CALIBRATION_MODE"               , 7,    MessageTypes::COMMAND}},
        {CMD_SET_ARUCO_MARKER_MODE              , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_ARUCO_MARKER_MODE"              , 7,    MessageTypes::COMMAND}},
        {CMD_GET_IP_ADDRESS                     , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_IP_ADDRESS"                     , 6,    MessageTypes::COMMAND}},
        {CMD_GET_ALL_IP_ADDRESSES               , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_ALL_IP_ADDRESSES"               , 6,    MessageTypes::COMMAND}},
        {CMD_GET_SAMPLE_RATE                    , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_SAMPLE_RATE"                    , 6,    MessageTypes::COMMAND}},
        {CMD_REMOVE_COMPLETE_QR_LIBRARY         , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_REMOVE_COMPLETE_QR_LIBRARY"         , 6,    MessageTypes::COMMAND}},
        {CMD_DOWNLOAD_QR_LIBRARY                , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_DOWNLOAD_QR_LIBRARY"                , 6,    MessageTypes::COMMAND}},
        {CMD_GET_ALL_SERIAL_NUMBERS             , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_ALL_SERIAL_NUMBERS"             , 6,    MessageTypes::COMMAND}},
        {CMD_REMOVE_COMPLETE_CLUSTER_LIBRARY    , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_REMOVE_COMPLETE_CLUSTER_LIBRARY"    , 6,    MessageTypes::COMMAND}},
        {CMD_GET_ALL_ACKNOWLEDGEMENTS           , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_ALL_ACKNOWLEDGEMENTS"           , 6,    MessageTypes::COMMAND}},
        {CMD_GET_SOFTWARE_VERSION               , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_SOFTWARE_VERSION"               , 6,    MessageTypes::COMMAND}},
        {CMD_GET_TCPIP_RECEIVER                 , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_TCPIP_RECEIVER"                 , 6,    MessageTypes::COMMAND}},
        {CMD_SET_SAMPLE_RATE                    , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_SAMPLE_RATE"                    , 8,    MessageTypes::COMMAND}},
        {CMD_REMOVE_QR_FROM_LIBRARY             , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_REMOVE_QR_FROM_LIBRARY"             , 8,    MessageTypes::COMMAND}},
        {CMD_SET_RECOVERY_MODE                  , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_RECOVERY_MODE"                  , 8,    MessageTypes::COMMAND}},
        {CMD_DELETE_CLUSTER                     , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_DELETE_CLUSTER"                     , 8,    MessageTypes::COMMAND}},
        {CMD_SET_IP_ADDRESS                     , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_IP_ADDRESS"                     , 18,   MessageTypes::COMMAND}},
        {CMD_SET_NEW_POSE                       , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_NEW_POSE"                       , 18,   MessageTypes::COMMAND}},
        {CMD_ADD_QR_TO_LIBRARY                  , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_ADD_QR_TO_LIBRARY"                  , 20,   MessageTypes::COMMAND}},
        {CMD_SET_TIME_DATE                      , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_TIME_DATE"                      , 13,   MessageTypes::COMMAND}},
        {CMD_SET_SENSOR_MOUNT_POSE              , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_SENSOR_MOUNT_POSE"              , 18,   MessageTypes::COMMAND}},
        {CMD_SET_POSE_AND_COVARIANCE            , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_POSE_AND_COVARIANCE"            , 38,   MessageTypes::COMMAND}},
        {CMD_START_MARKERLESS_LEARNING          , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_START_MARKERLESS_LEARNING"          , 9,    MessageTypes::COMMAND}},
        {CMD_SET_TCPIP_RECEIVER                 , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_TCPIP_RECEIVER"                 , 11,   MessageTypes::COMMAND}},
        {CMD_SET_LINE_FOLLOWER                  , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_START_LINE_FOLLOWER"                , 9,    MessageTypes::COMMAND}},
        {CMD_SET_UDP_SETTINGS                   , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_UDP_SETTINGS"                   , 12,   MessageTypes::COMMAND}},
        {CMD_GET_CLUSTER_G2O                    , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_CLUSTER_G2O"                    , 8,    MessageTypes::COMMAND}},
        {CMD_REPLACE_CLUSTER_G2O                , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_REPLACE_CLUSTER_G2O"                , 0,    MessageTypes::COMMAND}},
        {CMD_GET_SOFTWARE_HASH                  , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_SOFTWARE_HASH"                  , 6,    MessageTypes::COMMAND}}
    };

    const std::map<int, std::string> modesDescriptionMap{
        {0, "idle"},
        {1, "mapping"},
        {2, "relative tracking"},
        {3, "absolute localization"},
        {4, "pose recovery"},
        {5, "recording raw data"},
    };

    const std::map<int, std::string> warningsDescriptionMap{
        {0, "Too Fast For Mapping"},
        {1, "Too Fast For Localizing"},
        {2, "Low Surface Quality"},
        {3, "Low Throughput 1"},
        {4, "Low Throughput 2"},
        {5, "Low Throughput 3"},
        {6, "Low Memory"},
        {7, "Low Brightness Cam 1"},
        {8, "Low Brightness Cam 2"},
        {9, "High Brightness Cam 1"},
        {10, "High Brightness Cam 2"}
    };

    const std::map<int, std::string> errorsDescriptionMap{
        {0, "Error in Tracking"},
        {1, "Lost Position"},
        {2, "Connection Error"},
        {3, "License Error"},
        {4, "Security Error"},
        {5, "Connection Init Error"},
        {6, "Camera Timeout"},
        {7, "Camera Regrab"},
        {8, "Camera Consec"},
        {9, "Camera Other"},
        {10, "Camera Init"},
        {11, "Camera Count"},
        {12, "Camera Err Other"},
        {13, "Skipped Frame"},
        {14, "Map Sharing Error 1"},
        {15, "Map Sharing Error 2"},
        {16, "Map Sharing Error 3"},
        {17, "Map Sharing Error 4"},
        {18, "Map Sharing Error 5"},
        {19, "Map Sharing Error 6"},
    };

    const std::map<int, std::string> statusDescriptionMap{
        {0, "Above velocity threshold; ignoring commands"},
        {1, "Test Status 2"},
    };
};

#endif //COMMANDS_H_
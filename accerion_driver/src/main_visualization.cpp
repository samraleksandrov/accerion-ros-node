/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <ros/ros.h>

#include <Visualization.h>

int main(int argc, char *argv[])
{	
	ros::init(argc, argv, "accerion_visualization");

	Visualization accVisuals;

	ros::Rate rate(100);

	while(ros::ok())
	{
		ros::spinOnce();

		rate.sleep();
	}

	return 0;
}


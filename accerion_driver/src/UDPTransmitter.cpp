/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "UDPTransmitter.h"

using namespace std;

UDPTransmitter::UDPTransmitter(unsigned int remoteReceivePort)
{
    debugMode_ = false;

    remoteReceivePort_   = remoteReceivePort;

    socketEndpoint_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    int reuseAddress = 1;
    setsockopt(socketEndpoint_, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuseAddress, sizeof(reuseAddress));

    remoteAddress_.sin_port = htons(remoteReceivePort_);
    int broadcastEnable = 1;
    setsockopt(socketEndpoint_, SOL_SOCKET, SO_BROADCAST, (const char*)&broadcastEnable, sizeof(broadcastEnable));

    if(socketEndpoint_ < 0)
        cout << "Error while opening transmitting socket" << endl;
}

bool UDPTransmitter::setMulticastIPAddress(struct in_addr multicastAddress)
{
    struct in_addr netStart;
    struct in_addr netEnd;

    netStart.s_addr = inet_addr(NetworkConstants::multicastRangeFirstIPAddress.c_str());
    netEnd.s_addr   = inet_addr(NetworkConstants::multicastRangeLastIPAddress.c_str());

    if ((ntohl(multicastAddress.s_addr) >= ntohl(netStart.s_addr)) && (ntohl(multicastAddress.s_addr) <= ntohl(netEnd.s_addr)))
    {
        /*Set the IP addresses for multicast UDP message*/
        remoteAddress_.sin_family = AF_INET;
        remoteAddress_.sin_port = htons(remoteReceivePort_);
        remoteAddress_.sin_addr.s_addr = multicastAddress.s_addr;
        memset(remoteAddress_.sin_zero, '\0', sizeof(remoteAddress_.sin_zero));

        if (debugMode_)
        {
            cout << "From UDP Transmitter, setting multicast address to := " << inet_ntoa(remoteAddress_.sin_addr) << endl;
        }
        return true;
    }
    else
    {
        if(debugMode_)
        {
            std::cout << "False UDP Multicast address!" << std::endl;
        }
        return false;
    }
}

void UDPTransmitter::setBroadcastIPAddress(struct in_addr broadcastAddress)
{
    int broadcastEnable = 1;

    if (setsockopt(socketEndpoint_, SOL_SOCKET, SO_BROADCAST, (const char *)&broadcastEnable, sizeof(broadcastEnable)) == -1)
    {
        if (debugMode_)
            perror("Error while setting socket options for UDP transmitter, error is");
    }

    /*Set the IP addresses for broadcast UDP message*/
    remoteAddress_.sin_family = AF_INET;
    remoteAddress_.sin_port = htons(remoteReceivePort_);
    remoteAddress_.sin_addr.s_addr = broadcastAddress.s_addr;
    memset(remoteAddress_.sin_zero, '\0', sizeof(remoteAddress_.sin_zero));

    if (debugMode_)
    {
        cout << "From UDP Transmitter, setting broadcast address to := " << inet_ntoa(remoteAddress_.sin_addr) << endl;
    }
}

bool UDPTransmitter::transmitMessage(uint8_t* transmittedMessage, unsigned int transmittedNumOfBytes)
{
    if (transmittedNumOfBytes > bufferSize_)
    {
        cout << "Number of bytes is larger than maximum message size, number of bytes := " << transmittedNumOfBytes  << endl;
        return false;
    }
    if(sendto(socketEndpoint_, &transmittedMessage[0], transmittedNumOfBytes, 0, (struct sockaddr*) &remoteAddress_, sizeof(remoteAddress_)) == -1)
    {
        if(debugMode_)
            perror(" Error multicasting message to port, error is");
        return false;
    }
    return true;
}

UDPTransmitter::~UDPTransmitter()
{
    close(socketEndpoint_);
}


void UDPTransmitter::setReceiver(const std::string ip)
{
    remoteAddress_.sin_family = AF_INET;
    inet_pton(AF_INET, ip.data(), &(remoteAddress_.sin_addr));
    std::cout <<"UDPTransmitter setting receiver ip to " << ip << std::endl;
}
